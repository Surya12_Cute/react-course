import React from "react";
import ReactDOM from "react-dom";


import SearchBar from './components/search-bar'
import VideoList from './components/video-list';

import YTSearch from 'youtube-api-search';
const API_KEY = "AIzaSyBkNkzkEKN3N4HtNfoV5DDFkl4Q1_j36gk";



class Index extends React.Component{
	constructor(props){
		super(props);
		
		
		this.state = { video : [] };
		
		YTSearch({key : API_KEY ,term : "youtube rewind"}, (brian) => {
			this.setState({ video : brian });
			console.log(this.state.video)
		});
		
	}
	render(){
		return (
			<div>
				<SearchBar />
				<VideoList passingVideo={this.state.video} />
			</div>
		);
	};
};

ReactDOM.render(<Index />, document.getElementById("index"));