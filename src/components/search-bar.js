import React from 'react';

class SearchBar extends React.Component{
	constructor(props){
		super(props);
		
		this.state = { nilai : ""};
	}
	
	
	render() {
		
		return (
			<div>
				<input onChange={(kata) => { this.onInputChange(kata) }}/> 
				value of this input : {this.state.nilai}
			</div>
		);
	};
	onInputChange(cat) {
		this.setState({ nilai : cat.target.value })
	}
};

export default SearchBar;