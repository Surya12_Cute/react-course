import React from 'react';
import VideoListItem from './video-list-item';

const VideoList = (value) => {
	const VideoItems = value.passingVideo.map((videos) => {
		console.log(videos);
		return (
			<div key={videos.id.videoId}>
				<VideoListItem />
			</div>
		);
	});
	
	
	return (
		<div>
			{VideoItems}
		</div>
	);
		
}
export default VideoList;